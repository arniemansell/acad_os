#include "app.h"
#include "debug.h"
#include "version.h"

#include <QApplication>

int main(int argc, char *argv[]) {
  Q_INIT_RESOURCE(application);

  QApplication a(argc, argv);

  QCoreApplication::setOrganizationName(AUTHOR);

  QString appname = {APP_NAME};
  appname.append(" ");
  appname.append(VERSION);
  QCoreApplication::setApplicationName(appname);
  QCoreApplication::setApplicationVersion(VERSION);

  // Debug
  // int current_debug_level = dbg::NO_DEBUG;
  int current_debug_level = dbg::LVL2;
  dbg::init(current_debug_level);

  App app;
  app.setWindowIcon(QIcon(":images/mum.ico"));
  app.showMaximized();
  return a.exec();
}
