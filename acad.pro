QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

win: RC_ICONS += images/mum.ico

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    app/app.cpp \
    app/build.cpp \
    app/main.cpp \
    hpgl/hpgl.cpp \
    tabs/tabs.cpp \
    utils/ascii.cpp \
    utils/debug.cpp \
    utils/former.cpp \
    utils/object_oo.cpp \
    wing/airfoil.cpp \
    wing/element.cpp \
    wing/le_template.cpp \
    wing/part.cpp \
    wing/planform.cpp \
    wing/rib.cpp \
    wing/spar.cpp \
    wing/wing.cpp

HEADERS += \
    app/app.h \
    app/version.h \
    dxf/dxf.h \
    hpgl/hpgl.h \
    tabs/tabs.h \
    thirdparty/ascii.h \
    thirdparty/json/json.hpp \
    thirdparty/json/json_fwd.hpp \
    utils/debug.h \
    utils/former.h \
    utils/object_oo.h \
    wing/airfoil.h \
    wing/element.h \
    wing/le_template.h \
    wing/part.h \
    wing/planform.h \
    wing/rib.h \
    wing/spar.h \
    wing/wing.h

INCLUDEPATH += \
    $$PWD/app \
    $$PWD/dxf \
    $$PWD/hpgl \
    $$PWD/json \
    $$PWD/tabs \
    $$PWD/thirdparty \
    $$PWD/thirdparty/json \
    $$PWD/utils \
    $$PWD/wing

DISTFILES += \
   copyfiles/config.json

RESOURCES += \
   application.qrc

# Copy the contents of the copyfiles folder to the location of the executable
CONFIG(debug, debug|release) {
    VARIANT = debug
} else {
    VARIANT = release
}
# This makes the path platform independent
copydata.commands = $(COPY_DIR) \"$$shell_path($$PWD\\copyfiles)\" \"$$shell_path($$OUT_PWD\\$$VARIANT)\"
# Copy the files
first.depends = $(first) copydata
export(first.depends)
export(copydata.commands)
QMAKE_EXTRA_TARGETS += first copydata

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
