#pragma once

#include "object_oo.h"

#define N_SLICE 2000 // Number of vertical slices

typedef struct
{
  double x;
  double ymax;
  double ymin;
  double area;
} slice_t;

void neutralPoint(FILE **fp);
