#pragma once
/*
 Project - ACAD wing design software
 Author - Adrian Mansell
 License MIT - Copyright(c) 2019 - 2024 Adrian Mansell

 Permission is hereby  granted, free of charge, to any  person obtaining a copy
 of this softwareand associated  documentation files(the "Software"), to deal in
 the Software  without restriction, including without  limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and /or  sell copies
 of  the Software, and to  permit persons  to  whom  the Software  is furnished
 to do so, subject to the following conditions :
 The above copyright noticeand this permission notice shall be included in all
 copies or substantial portions of the Software.
 THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
 FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT.IN NO EVENT  SHALL THE
 AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM, DAMAGES OR  OTHER
 LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

#include "debug.h"
#include "object_oo.h"
#include "part.h"
#include "rib.h"
#include "tabs.h"

class Element : public Part {
public:
  enum shape_e {
    TUBE,
    BAR,
    DOT,
    NONE
  };

  enum z_e {
    CHOORD_L,
    SNAP_BOTTOM,
    SNAP_TOP,
    ROTATE_BOTTOM,
    ROTATE_TOP
  };

  shape_e shape = shape_e::NONE;
  double angle = 0.0;
  double diameter = 0.0;
  double width = 0.0;
  double depth = 0.0;
  double stX = 0.0;
  double stY = 0.0;
  double stZ = 0.0;
  double enX = 0.0;
  double enY = 0.0;
  double enZ = 0.0;
  bool widenSlots = false;
  z_e ztype = z_e::CHOORD_L;

  line yLn = {};
  line zLn = {};

  bool create(Rib_set &rbs, std::string &log, bool draftmode);

  obj &getPlan();
};

class Element_set {
public:
  std::list<Element> elms = {};
  bool draft = false;
  obj plan = {};

  /**
   * @brief Add elements from the entry model
   */
  bool add(GenericTab *T, std::string &log);

  /**
   * @brief Create the elements and add them to the ribs
   */
  bool create(Rib_set &rbs, std::string &log);

  /**
   * @brief Configure to work in draft mode
   */
  void draft_mode() {
    draft = true;
  }

  /**
   * @brief Get the plan view of the elements
   */
  obj &getPlan();
};
