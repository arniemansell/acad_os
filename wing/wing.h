#pragma once
/*
 Project - ACAD wing design software
 Author - Adrian Mansell
 License MIT - Copyright(c) 2019 - 2024 Adrian Mansell

 Permission is hereby  granted, free of charge, to any  person obtaining a copy
 of this softwareand associated  documentation files(the "Software"), to deal in
 the Software  without restriction, including without  limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and /or  sell copies
 of  the Software, and to  permit persons  to  whom  the Software  is furnished
 to do so, subject to the following conditions :
 The above copyright noticeand this permission notice shall be included in all
 copies or substantial portions of the Software.
 THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
 FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT.IN NO EVENT  SHALL THE
 AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM, DAMAGES OR  OTHER
 LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

#define _USE_MATH_DEFINES
#include <climits>
#include <cmath>
#include <stdio.h>
#include <unordered_map>
#include <vector>

#include "airfoil.h"
#include "element.h"
#include "le_template.h"
#include "object_oo.h"
#include "planform.h"
#include "rib.h"
#include "spar.h"

class Wing {
public:
  /**
   * @brief Get the plan for the wing
   */
  obj &getPlan();
  obj plan;

  /**
   * @brief Get the parts for the wing
   */
  obj &getParts();
  obj parts;

  /**
   * @brief Tell the wing it is to draw in draft mode
   */
  void setDraftMode();

  /**
   * @brief Export to HPGL
   */
  void exportToHpgl(QFileInfo &fi);

  /**
   * @brief Export to DXF
   */
  void exportToDxf(QFileInfo &fi);

  /**
   * @brief Export file basics
   */
  bool exportFileOpen(QFileInfo &fi, FILE **fd);

  Planform plnf;
  Airfoil_set aifs;
  Rib_set ribs;
  Spar_set sprs;
  Element_set elms;
  LeTemplate_set lets;

private:
};

void loadWingFiles(FILE **wfp);
