/*
 Project - ACAD wing design software
 Author - Adrian Mansell
 License MIT - Copyright(c) 2019 - 2024 Adrian Mansell

 Permission is hereby  granted, free of charge, to any  person obtaining a copy
 of this softwareand associated  documentation files(the "Software"), to deal in
 the Software  without restriction, including without  limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and /or  sell copies
 of  the Software, and to  permit persons  to  whom  the Software  is furnished
 to do so, subject to the following conditions :
 The above copyright noticeand this permission notice shall be included in all
 copies or substantial portions of the Software.
 THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
 FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT.IN NO EVENT  SHALL THE
 AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM, DAMAGES OR  OTHER
 LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

#include "planform.h"
#include "part.h"

Planform::Planform() {
  (void)addRole(LE);
  (void)addRole(TE);
  (void)addRole(BOX);
}

bool Planform::add(GenericTab *T, std::string &log) {
  DBGLVL2("Number planform parts %d", T->GetNumParts());
  for (int r = 0; r < T->GetNumParts(); r++) {
    DBGLVL1("Processing row %d of %s", r, T->GetKey().c_str());

    if (T->gqst(r, "LEORTE") == QString("LE"))
      addLePoint(T->gdbl(r, "X"), T->gdbl(r, "Y"));

    else if (T->gqst(r, "LEORTE") == QString("TE"))
      addTePoint(T->gdbl(r, "X"), T->gdbl(r, "Y"));

    else
      dbg::fatal(std::string("Unrecognised planform point type ") + __FILE__);
  }

  // Sanity checking
  if ((getRole(LE).size() == 0) || (getRole(LE).len() < 1.0)) {
    log.append("You have not defined a leading edge for the wing in the planform tab.\n");
    return false;
  }
  if ((getRole(TE).size() == 0) || (getRole(TE).len() < 1.0)) {
    log.append("You have not defined a trailing edge for the wing in the planform tab.\n");
    return false;
  }

  // Tidy up
  getRole(LE).del_zero_lens();
  getRole(LE).extend1mm();
  getRole(LE).extend1mm();
  getRole(TE).del_zero_lens();
  getRole(TE).extend1mm();
  getRole(TE).extend1mm();

  // Create the planform "Box"
  getRole(BOX).del();
  getRole(BOX).copy_from(getRole(LE));
  getRole(BOX).copy_from(getRole(TE));
  getRole(BOX).add(getRole(LE).get_sp(), getRole(TE).get_sp());
  getRole(BOX).add(getRole(LE).get_ep(), getRole(TE).get_ep());
  getRole(BOX).regularise();

  isDefined = true;
  return true;
}

void Planform::addLePoint(double x, double y) {
  getRole(LE).add(coord_t{x, y});
}

void Planform::addTePoint(double x, double y) {
  getRole(TE).add(coord_t{x, y});
}

obj &Planform::getPlan() {
  Part::getPlan().del();
  obj le(getRole(LE));
  obj te(getRole(TE));
  Part::getPlan().splice(le);
  Part::getPlan().splice(te);
  return Part::getPlan();
}

line Planform::get_airfoil_line(double leX, double teX) {
  coord_t le_pt, te_pt;
  line_iter dln;
  if ((!getRole(LE).top_intersect(leX, &le_pt, dln)) ||
      (!getRole(TE).top_intersect(teX, &te_pt, dln)))
    return line(coord_t{0, 0}, vector_t{0, 0});

  return line(te_pt, le_pt);
}

bool Planform::isInPlanform(coord_t pt) {
  if (getRole(BOX).size())
    return getRole(BOX).surrounds_point(pt);

  return false;
}

bool Planform::isInPlanform(const line &ln) {
  if (getRole(BOX).size()) {
    if (!getRole(BOX).surrounds_point(ln.get_S0()))
      return false;
    if (!getRole(BOX).surrounds_point(ln.get_S1()))
      return false;
    return true;
  }

  return false;
}
