#pragma once
/*
 Project - ACAD wing design software
 Author - Adrian Mansell
 License MIT - Copyright(c) 2019 - 2024 Adrian Mansell

 Permission is hereby  granted, free of charge, to any  person obtaining a copy
 of this softwareand associated  documentation files(the "Software"), to deal in
 the Software  without restriction, including without  limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and /or  sell copies
 of  the Software, and to  permit persons  to  whom  the Software  is furnished
 to do so, subject to the following conditions :
 The above copyright noticeand this permission notice shall be included in all
 copies or substantial portions of the Software.
 THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
 FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT.IN NO EVENT  SHALL THE
 AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM, DAMAGES OR  OTHER
 LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

#include "airfoil.h"
#include "debug.h"
#include "object_oo.h"
#include "part.h"
#include "planform.h"

class LeTemplate : public Part {
public:
  static constexpr double LE_TEMPLATE_DEPTH = 0.15; //<! Fraction of choord to include in LE templates
  double xpos = 0.0;                                //!< The position of the LE template
  line airfLn = {};                                 //!< Line between airfTE and airfLE
  bool create(Planform &pl, Airfoil_set &af, bool draftMode, std::string &log);
};

class LeTemplate_set {
public:
  bool draftMode = false;
  obj pparts = {};
  std::list<LeTemplate> lets = {};

  std::list<LeTemplate>::iterator begin() {
    return lets.begin();
  };
  std::list<LeTemplate>::iterator end() {
    return lets.end();
  };

  /**
   * @brief Add LE templates to the set from a generic tab
   */
  bool add(GenericTab *T, Planform &plnf, std::string &log);

  /**
   * @brief Create each spacer
   */
  bool create(Planform &pl, Airfoil_set &af, std::string &log);

  /**
   * @brief Configure to work in draft mode
   */
  void draft_mode() {
    draftMode = true;
  }

  /**
   * @brief Parts and their texts
   */
  void getPrettyParts(std::list<std::reference_wrapper<obj>> &objects, std::list<std::reference_wrapper<obj>> &texts);
};
