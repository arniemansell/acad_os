#pragma once
/*
 Project - ACAD wing design software
 Author - Adrian Mansell
 License MIT - Copyright(c) 2019 - 2024 Adrian Mansell

 Permission is hereby  granted, free of charge, to any  person obtaining a copy
 of this softwareand associated  documentation files(the "Software"), to deal in
 the Software  without restriction, including without  limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and /or  sell copies
 of  the Software, and to  permit persons  to  whom  the Software  is furnished
 to do so, subject to the following conditions :
 The above copyright noticeand this permission notice shall be included in all
 copies or substantial portions of the Software.
 THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
 FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT.IN NO EVENT  SHALL THE
 AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM, DAMAGES OR  OTHER
 LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

#include "object_oo.h"
#include "part.h"
#include "tabs.h"

class Planform : public Part {
public:
  static constexpr int LE = 0;
  static constexpr int TE = 1;
  static constexpr int BOX = 2;

  Planform();

  /**
   * @brief Add planform points from the entry tab
   */
  bool add(GenericTab *T, std::string &log);

  /**
   * @brief Return a line joining the trailing edge to leading edge at X positions
   */
  line get_airfoil_line(double leX, double teX);

  /**
   * @brief Is point/line within the box defined by the planform
   */
  bool isInPlanform(coord_t pt);
  bool isInPlanform(const line &ln);

  /**
   * @brief get an outline of the plan
   */
  obj &getPlan();

private:
  bool isDefined = false;
  void addLePoint(double x, double y);
  void addTePoint(double x, double y);
};
