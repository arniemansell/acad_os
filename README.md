# ACAD

ACAD is a Windows application for designing built-up model aircraft wings.  It has the following features to enable the creation of complex wing designs:

* Planform definition
* Airfoil definition
* Rib and spacer placements and definitions
* Geodetic rib placements
* Rib parameters such as washout, trailing edge thickness and manual slot keepouts
* Sheet spar and jig definitions
* Strip, box and H spar definitions
* Automated lightening hole creation
* Tube and bar section elements
* Wing sheeting jig definitions
* LE radius templates
* Preview tabs for the wing plan and the generated wing parts

For more information, see the User Guide in the docs directory.

# Installation

If you just want to use ACAD, an installer is provided for Windows 10 64-bit platforms.  Simply download and run the installer:

[Windows 10 64-bit Installer](https://bitbucket.org/arniemansell/acad_os/src/master/installer/)

If you want to run on a different platform you will need to build ACAD yourself.  ACAD is based on the Qt framework which supports Linux/MacOS/Windows platforms; however, I have only ever built and tested it on Windows 10 64-bit.


# Building ACAD from Source

Instructions in this section are for building from source for the Windows 10 64-bit platform.  QtCreator is used as the build environment, targetting the mingw-64 toolkit. 

## Dependencies

First, download and install [Qt](https://www.qt.io/). Currently ACAD uses:

* Qt 6.1.3
   * MinGW 8.1.0 64-bit Toolkit
* QtCreator 5.0.1
   * CDB Debugger Support
   * Debugging Tools for Windows
* Qt Installer Framework 4.1

The notes below assume Qt is installed to C:\Qt.

## Compiling and Running Using Qt Creator

* Start QtCreator
* Select <File><Open File or Project>
* Open the acad.pro file in the top level of the ACAD source
* Select the MinGW 8.1.0 64-bit toolkit
* Select <Build><Build Project "acad" for All Configurations>
* Once the build is complete you should be able to run ACAD from within QTCreator by selecting <Build><Run>

## Creating an Installer

ACAD uses the Qt Installer Framework to create its installer.  There are a couple of batch files to help with the creation.

* Update app/version.h with the new version number
* Update installer/config/config.xml with the new version number
* Update docs/User Guide.docx with the new version number, then print it to update User Guide.pdf
* Complete the release build as above
* Run the build from within QtCreator. Open each example file in turn and re-save in order to update their native version.
* Open a powershell in the source installer directory
* Run .\createinstall.bat
* AV scan the installer and save to the installer directory

The installer acad_installer.exe will be created in the installer directory.

The createinstall.bat script uses the cleaninstall.bat script to empty the package directories before copying the latest build files over.  You can use cleaninstall.bat directly to clear the package directories (for example, before making a git commit).

# Contribution guidelines

Please contact arnie@madeupmodels.com if you would like to contribute, or if you require a copy of the Qt source code.

# Code Guidelines

Code formatting follows LLVM and can be re-applied using clang-format:

clang-format -style="{BasedOnStyle: llvm, ColumnLimit: 0}"  -i *.cpp *.h

Code style is somewhat mixed; [Google C++ Style Guide](https://google.github.io/styleguide/cppguide.html) is a good start.

# Version History

3.0 First open-sourced version

3.1 Rib support tabs made fixed-width.  Width can be configured and marker squares for the rib outline are added every 10mm.

3.3 Add DXF export. New icons. (Restructure wing build sequence to support grouping in dxf exports; remove redundant spacer code.)

3.4 Girdiring tool tab.  Rib support tabs no longer cause issues with rib lightening holes and elements. Geodetics are no longer drawn reversed which was a problem when elements were included.
