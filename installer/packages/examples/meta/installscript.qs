function Component()
{
}

Component.prototype.createOperations = function()
{
    component.createOperations();
	
	if (systemInfo.productType === "windows") {
        component.addOperation("CreateShortcut", "@TargetDir@/examples", "@StartMenuDir@/Examples.lnk",
            "workingDirectory=@TargetDir@", "iconPath=%SystemRoot%/system32/SHELL32.dll",
            "iconId=2", "description=Example Files");	
        component.addOperation("CreateShortcut", "@TargetDir@/airfoils", "@StartMenuDir@/Airfoils.lnk",
            "workingDirectory=@TargetDir@", "iconPath=%SystemRoot%/system32/SHELL32.dll",
            "iconId=2", "description=Airfoil Dat Files");
	}
}
