set qt_bin=C:\Qt\6.1.3\mingw81_64\bin
set qt_binary_create=C:\Qt\Tools\QtInstallerFramework\4.1\bin\binarycreator.exe

REM Clean out old files
call %~dp0cleaninstall.bat

REM Copy in the documentation and examples
xcopy "%~dp0..\docs\User Guide.pdf" "%documentation%" /E /H /C /R /Q /Y
mkdir "%examples%\examples"
xcopy "%~dp0..\examples" "%examples%\examples" /E /H /C /R /Q /Y
mkdir "%examples%\airfoils"
xcopy "%~dp0..\airfoils" "%examples%\airfoils" /E /H /C /R /Q /Y

REM Copy in the executable and config file and license
set release_appdir=%~dp0..\..\build-acad-Desktop_Qt_6_1_3_MinGW_64_bit-Release\release
xcopy "%release_appdir%\acad.exe" "%application%" /E /H /C /R /Q /Y
xcopy "%release_appdir%\config.json" "%application%" /E /H /C /R /Q /Y
xcopy "%~dp0..\docs\license.txt" "%application_meta%" /E /H /C /R /Q /Y
xcopy "%~dp0..\images\mum.ico" "%application%" /E /H /C /R /Q /Y

REM Windows deploy tool for dependencies
call "%qt_bin%\qtenv2.bat"
cmd /C "%qt_bin%\windeployqt.exe %application%\acad.exe"

REM Create the installer
cd %~dp0
cmd /C "%qt_binary_create% --offline-only -c .\config\config.xml -p packages acad_installer.exe"
