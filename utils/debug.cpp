/*
 Project - ACAD wing design software
 Author - Adrian Mansell
 License MIT - Copyright(c) 2019 - 2024 Adrian Mansell

 Permission is hereby  granted, free of charge, to any  person obtaining a copy
 of this softwareand associated  documentation files(the "Software"), to deal in
 the Software  without restriction, including without  limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and /or  sell copies
 of  the Software, and to  permit persons  to  whom  the Software  is furnished
 to do so, subject to the following conditions :
 The above copyright noticeand this permission notice shall be included in all
 copies or substantial portions of the Software.
 THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
 FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT.IN NO EVENT  SHALL THE
 AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM, DAMAGES OR  OTHER
 LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

#define _CRT_SECURE_NO_WARNINGS
#include "debug.h"
#include <assert.h>
#include <stdio.h>

/*
 * New Debug code for GUI
 */
int dbg::dbglvl = dbg::NO_DEBUG;
bool dbg::dbgOpen = false;
std::ofstream dbg::fdbg = {};

void dbg::init(int lvl) {
  dbglvl = lvl;

  if (dbglvl > NO_DEBUG) {
    if (!dbgOpen) {
      std::string fname = (QDir::homePath().append("/Documents/acad/acad.log")).toStdString();
      dbg::fdbg.open(fname, std::ios::out | std::ios::trunc);
      if (!dbg::fdbg.is_open()) {
        throw std::runtime_error(std::string("Unable to open debug log file ") + fname);
      }
      dbgOpen = true;
    }
  }
}

void dbg::lvl1(std::string const &str) {
  if (dbglvl >= LVL1)
    if (dbgOpen)
      fdbg << str << std::endl;
}

void dbg::lvl1(char *str) {
  if (dbglvl >= LVL1)
    if (dbgOpen)
      fdbg << std::string(str) << std::endl;
}

void dbg::lvl2(char *str) {
  if (dbglvl >= LVL2)
    if (dbgOpen)
      fdbg << std::string(str) << std::endl;
}

void dbg::lvl2(std::string const &str) {
  if (dbglvl >= LVL2)
    if (dbgOpen)
      fdbg << str << std::endl;
}

void dbg::alert(std::string const &str, std::string const &details) {
  QMessageBox mb;
  mb.setText(QString::fromStdString(str));
  if (!details.empty())
    mb.setDetailedText(QString::fromStdString(details));
  mb.setIcon(QMessageBox::Warning);
  mb.exec();
}

void dbg::fatal(std::string const &str, std::string const &details) {
  QMessageBox mb;
  mb.setText(QString::fromStdString(str));
  mb.setInformativeText(QString("ACAD will exit..."));
  if (!details.empty())
    mb.setDetailedText(QString::fromStdString(details));
  mb.setIcon(QMessageBox::Critical);
  mb.exec();
  if (dbgOpen)
    fdbg.close();
  exit(0);
}
